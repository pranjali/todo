"""
Author: Pranjali Pokharel
File contains the methods to make changes to our Item model
Methods to create new To-Do Item, edit existing Item, or destroy an Item are here
"""


class ItemsController < ApplicationController
  def new
    # Method takes to the new To-Do item view
    @item = Item.new
    render :'items/show_form'
  end

  def create
    # Method creates a new To-Do Item with form data
    @item = Item.new(item_params)
    save_item
  end

  def edit
    # Method allows the user to edit an existing To-Do item
    @item = Item.find(params[:id])
    render :'items/show_form'
  end

  def update
    # Method to update the edited item data
    @item = Item.find(params[:id])
    @item.assign_attributes(item_params)
    save_item
  end

  def complete
    # Method to complete the To-Do item
    @item = Item.find(params[:id])
    @item.completed= TRUE
    @item.completed_on= Date.today
    @item.save
    @items = Item.all
    render :'items/reload_items'
  end

  def destroy
    # Method allow the user to delete the To-Do item
    @item = Item.find(params[:id])
    @item.destroy
    @items = Item.all
    render :'items/reload_items'
  end

  private
  def save_item
    if @item.save
      @items = Item.all
      render :'items/hide_form'
    else
      render :'items/show_form'
    end
  end

  def item_params
    # Form parameters of the item model form
    params.require(:item).permit(:title, :description, :due_date)
  end
end
