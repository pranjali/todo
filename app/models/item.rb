"""
Author: Pranjali Pokharel
Item model validations for title and its methods
"""


class Item < ApplicationRecord
  validates_presence_of :title
  validate :past_due_date

  private
  def past_due_date
    # Throw error if the item due date is in the past
    if !due_date.blank? && due_date < Date.today
      self.errors.add(:due_date, "Due date cannot be in the past!")
    end
  end
end
