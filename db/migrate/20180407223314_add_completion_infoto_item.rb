class AddCompletionInfotoItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :completed, :boolean, null: false, default: false
    add_column :items, :completed_on, :date
  end
end
