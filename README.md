# README

A simple To-Do app. 

Please, make sure you have the following requirements installed before running the app. 

* Ruby version: 2.4.4

* Rails version: 5.2.0.rc2 

* bundler (for installing gems)

### Building & Running

To run this application, first download this repository and save it onto your computer. 
Go to the root directory of the project and run the following command to install
the required gems. 

```
bundle install
```

Then, run the database setup with the following command:

```
rake db:setup
```

To run the application server enter the following command into the terminal

```
rails server
```

At this point you can access the web application via your localhost url at port 3000

```
http://localhost:3000/
```

However, as you can see there is no data in the database and you will be greeted with an empty To-Do list.
At this point you can go ahead and add a new To-Do Item from your browser. 
From there you have the option of editing the item, marking it as complete or deleting it. 

### [User Guide](https://bitbucket.org/pranjali/todo/wiki/Home)

### [Testing Docs](https://bitbucket.org/pranjali/todo/wiki/Testing)

### [Class and Public Methods Docs](https://bitbucket.org/pranjali/todo/wiki/Class%20and%20Public%20Methods%20Documentation)
