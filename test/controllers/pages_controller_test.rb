"""
Author: Pranjali Pokharel
Test for the Pages Controller (Home/Index Page)
"

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    # Test to make sure a get request on the home page returns success
    get root_url
    assert_response :success
  end
end
