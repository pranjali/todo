"""
Author: Pranjali Pokharel
Test for the Item model
"
require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    # Set up the item being used for testing. Grab info from fixture
    @item_invalid = items(:invalid)
    @item_valid = items(:valid)
    @item_past_due = items(:pastdue)
  end

  test "title required" do
    # Validation Testing: Test to make sure item does not save if title not provided
    assert_not @item_invalid.save
  end

  test "past due" do
    # Validation Testing: Test to make sure the error of due date comes up if due date is in the past
    assert_not_nil @item_past_due.errors[:due_date], "Due date cannot be in the past!"
  end

  test "create an item" do
    # Make sure a To-Do item saves with the edited title provided
    assert @item_valid.save
  end

  test "edit an item" do
    # Make sure you can edit a previously valid item and save it with the new data
    @item_valid.title = "Edited Title"
    assert @item_valid.save
    assert_equal(@item_valid.title, "Edited Title")
  end

  test "delete an item" do
    # Make sure a To-do item is deleted with the destroy command
    @item_valid.destroy
    assert @item_valid.destroyed?
  end
end
