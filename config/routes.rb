Rails.application.routes.draw do
 root to: 'pages#home'
 resources :items, except: [:index] do
   member do
     post 'complete'
   end
 end
end
